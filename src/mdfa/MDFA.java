package mdfa;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;


import nfa.Estado;
import dfa.DFA;
import dfa.TablaDFA;

public class MDFA {
    private Map<Integer, TablaDFA> DTran;
    private Map<Integer,Estado> estadosDFA;
    private List<Character> alfabeto;
    private Integer EstadoInicialDFA;
    private List<Estado> estadosfinalesDFA;
    
    private Map<Integer, Set<Integer>> conjuntoPI;
    private Map<Integer,Estado> estadosMDFA;
    private Map<String, Integer> Grupo;
    private int InicialMDFA;
    private Set<Estado> FinalesMDFA;
    private Map<Integer, TablaDFA> Transiciones;
    private int nomestado=-1;

    private Map<String, String> TablaSimbolos;
    
    public MDFA(DFA dfa) {
        this.DTran = dfa.getTablaTransiciones();
        this.estadosDFA = dfa.getEstados();
        this.alfabeto = dfa.getAlfabeto();
        this.EstadoInicialDFA = dfa.getEstadoInicial();
        this.estadosfinalesDFA = dfa.getEstadosFinales();
        
        this.conjuntoPI = new HashMap<Integer, Set<Integer>>();
        this.estadosMDFA = new HashMap<Integer,Estado>();
        this.Grupo = new HashMap<String, Integer>();
        this.FinalesMDFA = new HashSet<Estado>();
        this.Transiciones = new HashMap<Integer, TablaDFA>();
        this.TablaSimbolos = new HashMap<String, String>();
        minimizar();
    }

    public void minimizar() {
        inicializarPI();
        boolean haycambio=true;
        Map<Integer, Set<Integer>> PInueva = this.conjuntoPI;
        //int i='A';
        int i=0;
        Map<Integer, Set<Integer>> destinos = new HashMap<Integer, Set<Integer>>();
        while (i <= this.nomestado) {
            haycambio = false;
            Set<Integer> conjunto = PInueva.get(i);
            if (conjunto.size() > 1) {
                int k = 0;
                while(k<this.alfabeto.size() && !haycambio){
                    Character a = this.alfabeto.get(k);
                    destinos.clear();
                    for (Integer estado : conjunto) {
                        int x = this.DTran.get(estado).getEstado(Character.toString(a));
                        Estado ex = this.estadosDFA.get(x);
                        if (!destinos.containsKey(ex.getgrupo())) {
                            Set<Integer> s1 = new HashSet<Integer>();
                            destinos.put(ex.getgrupo(), s1);
                        }
                        destinos.get(ex.getgrupo()).add(estado);
                    }
                    if (destinos.size() > 1) {
                        haycambio = true;
                        PInueva = separar(PInueva, destinos, i);
                        i = -1;
                    }
                    k++;
                }
            }
            i++;
        }
        this.conjuntoPI = PInueva;
        crearTabla();
    }

    public void inicializarPI() {
        if (this.conjuntoPI.isEmpty()) {
            for (int i = 'A'; i <= DFA.nomEstado; i++) {
                if (this.estadosDFA.get(i).getToken() == null) {
                    if (!this.Grupo.containsKey("null")) {
                        this.Grupo.put("null", ++this.nomestado);
                        Set<Integer> s1 = new HashSet<Integer>();
                        this.conjuntoPI.put(this.nomestado, s1);
                    }
                    this.conjuntoPI.get(this.Grupo.get("null")).add(this.estadosDFA.get(i).getid());
                    this.estadosDFA.get(i).setgrupo(this.Grupo.get("null"));
                } else {
                    if (!this.Grupo.containsKey(this.estadosDFA.get(i).getToken())) {
                        this.Grupo.put(this.estadosDFA.get(i).getToken(), ++this.nomestado);
                        Set<Integer> s1 = new HashSet<Integer>();
                        this.conjuntoPI.put(this.nomestado, s1);
                    }
                    this.conjuntoPI.get(this.Grupo.get(this.estadosDFA.get(i).getToken()))
                            .add(this.estadosDFA.get(i).getid());
                    this.estadosDFA.get(i).setgrupo(this.Grupo.get(this.estadosDFA.get(i).getToken()));
                }
            }
        }
    }

    public void crearTabla() {
        for (int i = 0; i <= this.nomestado; i++) {
            Estado e1 = new Estado(i, true);
            this.estadosMDFA.put(i, e1);
            TablaDFA t1 = new TablaDFA(this.alfabeto);
            ArrayList<Integer> a1=new ArrayList<Integer>();
            a1.addAll(conjuntoPI.get(i));
            int x = a1.get(0);
            for (Character a : this.alfabeto) {
                int sig=this.DTran.get(x).getEstado(Character.toString(a));
                t1.addEstado(Character.toString(a), this.estadosDFA.get(sig).getgrupo());
            }
            this.Transiciones.put(i, t1);
            if (this.conjuntoPI.get(i).contains(this.EstadoInicialDFA)) {
                setEstadoInicial(i);
            }
        }
        setEstadosFinales();
    }

    public Map<Integer, Set<Integer>> separar(Map<Integer, Set<Integer>> pi, Map<Integer, Set<Integer>> destinos, int k) {
        pi.put(k, new HashSet<Integer>());
        int i = 0;
        for (Integer key : destinos.keySet()) {
            if (i==0) {
                pi.get(k).addAll(destinos.get(key));
                ArrayList<Integer> a = new ArrayList<Integer>();
                a.addAll(pi.get(k));
                for (int j = 0; j < pi.get(k).size(); j++) {
                    this.estadosDFA.get(a.get(j)).setgrupo(k);
                }
                i++;
            } else {
                pi.put(++this.nomestado, new HashSet<Integer>());
                pi.get(this.nomestado).addAll(destinos.get(key));
                ArrayList<Integer> a = new ArrayList<Integer>();
                a.addAll(pi.get(this.nomestado));
                for (int j = 0; j < pi.get(this.nomestado).size(); j++) {
                    this.estadosDFA.get(a.get(j)).setgrupo(this.nomestado);
                }
            }
        }
        return pi;
    }
    
    private void setEstadoInicial(Integer ini) {
        InicialMDFA = ini;
        this.estadosMDFA.get(ini).definirInicial(true);
    }

    public Integer getEstadoInicial() {
        return this.InicialMDFA;
    }

    private void setEstadosFinales() {
        for (Estado e : this.estadosfinalesDFA) {
            for (int i = 0; i <= this.nomestado; i++) {
                if (this.conjuntoPI.get(i).contains(e.getid())) {
                    this.estadosMDFA.get(i).definirFinal(true);
                    this.estadosMDFA.get(i).definirToken(e.getToken());
                    this.FinalesMDFA.add(this.estadosMDFA.get(i));
                }
            }
        }
    }

    public void validar(String cadena) {
        cadena = cadena + " ";
        boolean m = false;
        Integer e = this.getEstadoInicial();
        String c = Character.toString(cadena.charAt(0));
        String lexema = c;
        for (int i = 1; i < cadena.length(); i++) {
            System.out.print("\033[H\033[2J");
            System.out.println(cadena);
            System.out.println(String.format("%"+i+"s", "▲"));
            if (this.alfabeto.contains(c.charAt(0)) || c.compareTo(" ")==0) {
                e = this.Transiciones.get(e).getEstado(c);
                c = Character.toString(cadena.charAt(i));
                if (c.compareTo(" ") == 0) {
                    if (this.estadosMDFA.get(e).getFinal()) {
                        this.TablaSimbolos.put(lexema, this.estadosMDFA.get(e).getToken());
                        System.out.println("");
                        System.out.println("Se ha encontrado un lexema valido");
                        System.out.println("Lexema: " + lexema);
                        System.out.println("Token: " + this.estadosMDFA.get(e).getToken());
                        System.out.println("");
                        e = this.getEstadoInicial();
                        if (i < cadena.length() - 1)
                            c = Character.toString(cadena.charAt(++i));
                        lexema = c;
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException err) {
                            err.printStackTrace();
                        }                        
                    } else {
                        m = true;
                    }
                } else
                    lexema = lexema + c;
            } else {
                m = true;
            }
            if (m) {
                System.out.println("!!!");
                System.out.println("Cadena de caracteres no identificada");
                System.out.println("Cadena: " + lexema);
                System.out.println("!!!");
                e = this.getEstadoInicial();
                if (i < cadena.length() - 1)
                    c = Character.toString(cadena.charAt(++i));
                lexema = c;
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException err) {
                    err.printStackTrace();
                }
                m = false;
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException err) {
                err.printStackTrace();
            }
        }
    }

    public void imprimir() {
        try {
            PrintWriter writer = new PrintWriter("MDFA.txt", "UTF-8");
            String s;
            //writer.println("DEFINICIÓN REGULAR: ");
            //imprimirDefinicion(this.der, writer);
            writer.println("--------------------------------------------------------------");
            writer.println("");
            writer.println("");
            writer.println("--------------------------DFA Minimo--------------------------");
            writer.println("-----------Generado por el algoritmo de minimizacion----------");
            String r;
            String t;
            s = "   |     ||";
            for (Character c : alfabeto) {
                s = s + "   " + c + "   |";
            }
            r = "   =";
            t = "   -";
            for (int i = 0; i < s.length() - 4; i++) {
                r = r + "=";
                t = t + "-";
            }
            writer.println(r);
            writer.println(s);
            writer.println(r);
            r = "";
            for (int i = 0; i <= this.nomestado; i++) {
                s = "   |  " + (char) (i+65) + "  ||";
                r = r + "\n" + (char) (i+65) + " = " + this.conjuntoPI.get(i);
                TablaDFA aux = this.Transiciones.get(i);
                for (Character c : alfabeto) {
                    if (aux.getEstado(Character.toString(c)) == null) {
                        s = s + "       |";
                    } else {
                        int n = 'A' +(int)aux.getEstado(Character.toString(c));
                        s = s + "   " + (char) n + "   |";
                    }
                }
                writer.println(s);
                writer.println(t);
            }
            writer.println(r);
            int n = this.InicialMDFA + 65;
            writer.println("ESTADO INICIAL: " + (char) n);
            writer.println("ESTADOS FINALES DE TOKENS");
            for (Estado e : this.FinalesMDFA) {
                writer.println("[" + (char) (e.getid()+65) + "] = " + e.getToken());
            }
            writer.println("--------------------------DFA Minimo--------------------------");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public void imprimirResultados() {
        try {
            PrintWriter writer = new PrintWriter("Resultados.txt", "UTF-8");
            String s;
            writer.println("------------------------------------------------------------------------");
            writer.println("");
            writer.println("-------------------------------Resultados-------------------------------");
            writer.println("--Generado al iterar sobre el afd mínimo con los caracteres de entrada--");
            writer.println("------------------------------------------------------------------------");
            writer.println("");
            writer.println("");
            String r;
            String le;
            String to;
            String t;
            s = "   |";
            le = "      Lexema      |";
            s = s + le;
            to = "        Token        |";
            s = s + to;
            r = "   =";
            t = "   -";
            for (int i = 0; i < s.length() - 4; i++) {
                r = r + "=";
                t = t + "-";
            }
            writer.println(r);
            writer.println(s);
            writer.println(r);
            for (String x : this.TablaSimbolos.keySet()) {
                s = "   |  ";
                s = s + x;
                for (int i = x.length()+2; i < le.length()-1; i++) {
                    s = s + " ";
                }
                s = s + "|  ";
                s = s + this.TablaSimbolos.get(x);
                for (int i = this.TablaSimbolos.get(x).length()+2; i < to.length()-1; i++) {
                    s = s + " ";
                }
                s = s + "|";
                writer.println(s);
                writer.println(t);
            }
            writer.println("");
            writer.println("");
            writer.println("-------------------------------Resultados-------------------------------");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
