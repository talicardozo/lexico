package dfa;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import nfa.Estado;
import nfa.Tabla;

public class DFA {
    private Map<Integer, Tabla> transicionesNFA;
    private List<Estado> estadosfinalesNFA;
    private List<Character> alfabeto;
    private Integer inicialNFA;
    
    private Map<Integer, Set<Integer>> Destados;
    private Map<Integer, TablaDFA> DTran;
    private Set<Integer> conjunto;
    public static int nomEstado = 'A';
    private Integer EstadoInicialDFA;
    private Map<Integer,Estado> estadosDFA;
    private List<Estado> estadosfinalesDFA;

    public DFA(Map<Integer, Tabla> transiciones, List<Estado> estados, Integer inicial, List<Character> alfabeto) {
        this.transicionesNFA = transiciones;
        this.estadosfinalesNFA = estados;
        this.alfabeto = alfabeto;
        this.inicialNFA = inicial;
        if (this.alfabeto.contains('$')) 
            this.alfabeto.remove(this.alfabeto.indexOf('$'));
        
        this.Destados = new HashMap<Integer, Set<Integer>>();
        this.DTran = new HashMap<Integer, TablaDFA>();
        this.estadosDFA = new HashMap<Integer, Estado>();
        this.estadosfinalesDFA= new ArrayList<Estado>();
        this.subconjuntos();
    }

    public void subconjuntos() {
        TablaDFA t1 = new TablaDFA(this.alfabeto);

        this.conjunto = mover(this.inicialNFA, '$');
        Destados.put(nomEstado, conjunto);

        Estado nuevo = new Estado(nomEstado, true);
        this.estadosDFA.put(nomEstado, nuevo);
        
        this.DTran.put(nomEstado, t1);
        int marcado = nomEstado;
        setEstadoInicial(nomEstado);
        while (nomEstado >= marcado) {
            Set<Integer> T = Destados.get(marcado);
            for (Character a : this.alfabeto) {
                Set<Integer> U = mover(mover(T, a), '$');
                Integer est = estaenDestados(U);
                if (est == nomEstado) {
                    Destados.put(nomEstado, U);
                    nuevo = new Estado(nomEstado, true);
                    this.estadosDFA.put(nomEstado, nuevo);
                    t1 = new TablaDFA(this.alfabeto);
                    this.DTran.put(nomEstado, t1);
                }
                this.DTran.get(marcado).addEstado(Character.toString(a), est);
            }
            marcado++;
        }
        setEstadosFinales();
    }
    
    private Set<Integer> mover(Integer inicial, Character simbolo) {
        Set<Integer> N = new HashSet<Integer>();
        List<Integer> t1 = new ArrayList<Integer>();
        t1.addAll(this.transicionesNFA.get(inicial).getEstadoSet(Character.toString(simbolo)));
        if (simbolo=='$') {
            N.add(inicial);
        }
        int c=0;
        int cantidad = t1.size();
        while(c<cantidad) {
            Integer estado = t1.get(c);
            if (!N.contains(estado)) {
                N.add(estado);
                t1.addAll(this.transicionesNFA.get(estado).getEstadoSet(Character.toString(simbolo)));
                cantidad = t1.size();
            }
            c = c + 1;
        }

        return N;
    }
    
    private Set<Integer> mover(Set<Integer> T, Character simbolo) {
        Set<Integer> N = new HashSet<Integer>();
        for (Integer i : T) {
            if (!this.transicionesNFA.get(i).getEstadoSet(Character.toString(simbolo)).isEmpty())
                N.addAll(mover(i, simbolo));
        }
        return N;
    }

    private Integer estaenDestados(Set<Integer> U) {
        for (int i = 'A'; i < nomEstado; i++) {
            if (this.Destados.get(i).equals(U)) {
                return i;
            }
        }
        nomEstado++;
        return nomEstado;
    }

    private void setEstadoInicial(Integer ini) {
        EstadoInicialDFA = ini;
        this.estadosDFA.get(ini).definirInicial(true);
    }

    public int getEstadoInicial() {
        return this.EstadoInicialDFA;
    }

    private void setEstadosFinales() {
        for (Estado e : this.estadosfinalesNFA) {
            for (int i = 'A'; i <= nomEstado; i++) {
                if (this.Destados.get(i).contains(e.getid())) {
                    this.estadosDFA.get(i).definirFinal(true);
                    this.estadosDFA.get(i).definirToken(e.getToken());
                    this.estadosfinalesDFA.add(this.estadosDFA.get(i));
                }
            }
        }
    }

    public void imprimir() {
        try {
            PrintWriter writer = new PrintWriter("DFA.txt", "UTF-8");
            String s;
            writer.println("DEFINICIÓN REGULAR: ");
            //imprimirDefinicion(this.der, writer);
            writer.println("--------------------------------------------------------------");
            writer.println("");
            writer.println("");
            writer.println("----------DFA Generado con Algoritmo de Subconjuntos----------");
            String r;
            String t;
            s = "   |     ||";
            for (Character c : alfabeto) {
                s = s + "   " + c + "   |";
            }
            r = "   =";
            t = "   -";
            for (int i = 0; i < s.length() - 4; i++) {
                r = r + "=";
                t = t + "-";
            }
            writer.println(r);
            writer.println(s);
            writer.println(r);
            r = "";
            for (int i = 'A'; i <= nomEstado; i++) {
                s = "   |  " + (char) i + "  ||";
                r = r + "\n" + (char) i + " = " + this.Destados.get(i);
                TablaDFA aux = DTran.get(i);
                for (Character c : alfabeto) {
                    if (aux.getEstado(Character.toString(c)) == null) {
                        s = s + "       |";
                    } else {
                        int n = aux.getEstado(Character.toString(c));
                        s = s + "   " + (char) n + "   |";
                    }
                }
                writer.println(s);
                writer.println(t);
            }
            writer.println(r);
            int n = this.EstadoInicialDFA;
            writer.println("ESTADO INICIAL: " + (char) n);
            writer.println("ESTADOS FINALES DE TOKENS");
            for (Estado e : this.estadosfinalesDFA) {
                writer.println("[" + (char) e.getid() + "] = " + e.getToken());
            }
            writer.println("-----------------------DFA------------------------");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Character> getAlfabeto() {
        return this.alfabeto;
    }

    public Map<Integer, TablaDFA> getTablaTransiciones() {
        return this.DTran;
    }

    public Map<Integer,Estado> getEstados() {
        return this.estadosDFA;
    }

    public List<Estado> getEstadosFinales() {
        return this.estadosfinalesDFA;
    }
}
