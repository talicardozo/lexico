package dfa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TablaDFA {
    private Map<String, Integer> tabla;

    public TablaDFA(List<Character> alfabeto) {
        this.tabla = new HashMap<String, Integer>();
        for (Character c : alfabeto) {
            this.tabla.put(Character.toString(c), null);
        }
    }

    public void addEstado(String a, Integer e) {
        this.tabla.put(a,e);
    }

    public Integer getEstado(String a) {
        return this.tabla.get(a);
    }
    
    public Integer getLongitud() {
        return this.tabla.size();
    }

    public Map<String, Integer> getTabla() { return this.tabla; }
}