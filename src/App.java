import java.util.Scanner;

import dfa.DFA;
import mdfa.MDFA;

import java.util.ArrayList;
import nfa.NFA;

/* 
variable->letra(letra|digito)
letra->[a-z]
digito->[a-z]
*/
public class App {
    public static void main(String[] args) throws Exception {
        ArrayList<String> expresiones = new ArrayList<String>();
        ArrayList<Character> alfabeto = new ArrayList<Character>();
        System.out.print("\033[H\033[2J");
        System.out.println("Ejemplo 1: ");
        System.out.println("         exp->(letra*).a.b.b");
        System.out.println("         letra->a|b");
        System.out.println("");
        System.out.println("Ejemplo 2: ");
        System.out.println("         parentesis->[|]");
        System.out.println("         letra->x|y|z");
        System.out.println("         numero->0|1|2|3|4|5|6|7|8|9");
        System.out.println("         operador->+|-");
        System.out.println("");
        System.out.println("Obs: el caracter $ es utilizado como simbolo de vacio");
        System.out.println("");
        System.out.println("");
        System.out.println("Ingrese una a una las expresiones de la definicion del alfabeto | $ para finalizar");
        Scanner in = new Scanner(System.in);
        String re = in.nextLine();
        do {
            expresiones.add(re);
            re = in.nextLine();
        } while (re.compareTo("$") != 0);
        
        System.out.println("");
        System.out.println("");
        System.out.println("Ingrese una a una los caracteres que pertenecen al alfabeto | $ para finalizar");
        do {
            re = in.nextLine();
            alfabeto.add(re.charAt(0));
        } while (re.compareTo("$") != 0);
        
        NFA nfa = new NFA(expresiones,alfabeto);
        nfa.imprimir();
        System.out.println("");
        System.out.println("AFN resultante guardado en AFN.txt");
        System.out.println("");
        
        DFA dfa = new DFA(NFA.transicionesEstados, nfa.getEstadosFinales(), nfa.estadoInicialAFN.getid(), NFA.alfabeto);
        dfa.imprimir();
        System.out.println("");
        System.out.println("DFA resultante guardado en DFA.txt");
        System.out.println("");
        
        MDFA mdfa = new MDFA(dfa);
        mdfa.imprimir();
        System.out.println("");
        System.out.println("MDFA resultante guardado en MDFA.txt");
        System.out.println("");
        System.out.println("-------------------------");
        System.out.println("");
        System.out.println("Ingrese cadena a validar | $ para terminar");
        System.out.println("");
        re = in.nextLine();
        do {
            mdfa.validar(re);
            System.out.println("");
            System.out.println("Ingrese cadena a validar | $ para terminar");
            System.out.println("");
            re = in.nextLine();
        } while (re.compareTo("$") != 0);
        mdfa.imprimirResultados();
        System.out.println("");
        System.out.println("Resultado del analisis lexico guardado en Resultado.txt");
        System.out.println("");

        in.close();
    }
}
