package nfa;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;


import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.List;

public class NFA {
    private List<String> expresiones;
    public static List<Character> alfabeto;
    private List<String> izq;
    private List<String> der;
    private List<String> derposfijo;
    public Estado estadoInicialAFN;
    public Estado estadoFinalAFN;
    private List<Estado> estadosfinales;
    public static int nroestados;
    
    private static Map <Character, Integer> precedenciaOper;
    public static Map<Integer, Tabla> transicionesEstados;
    public static List<Estado> estados;
    private Stack<Estado> pilainiciales;
    private Stack<Estado> pilafinales;
    private Estado auxfinal;

    public NFA(ArrayList<String> expresiones, ArrayList<Character> alfa) {
        Map<Character, Integer> map = new HashMap<Character, Integer>();
        map.put('(', 1);
        map.put('|', 2);
        map.put('.', 3);
        map.put('*', 4);
        map.put('+', 4);
        precedenciaOper = Collections.unmodifiableMap(map);
        
        nroestados = 0;
        NFA.alfabeto = alfa;
        this.expresiones = expresiones;
        this.derposfijo = new ArrayList<String>();
        this.estadosfinales = new ArrayList<Estado>();
        this.izqder();

        for (int i = 0; i < der.size(); i++) {
            this.derposfijo.add(aposfijo(this.der.get(i)));
        }

        NFA.transicionesEstados = new HashMap<Integer, Tabla>();
        NFA.estados = new ArrayList<Estado>();
        this.pilafinales = new Stack<Estado>();
        this.pilainiciales = new Stack<Estado>();
        this.thompson();
    }

    private void izqder() {
        this.izq = new ArrayList<String>();
        this.der = new ArrayList<String>();

        for (int i = 0; i < this.expresiones.size(); i++) {
            int fle = this.expresiones.get(i).indexOf("->");
            int fin = this.expresiones.get(i).length();
            String izquierdo = this.expresiones.get(i).substring(0, fle);
            String derecho = this.expresiones.get(i).substring(fle + 2, fin);
            izq.add(izquierdo);
            der.add(derecho);
        }
        for (int i = 0; i < this.der.size(); i++) {

            for (int k = 1; k < this.der.size(); k++) {
                String mayor = this.der.get(i);
                String menor = this.izq.get(k);
                this.izq.get(k);
                int ini = mayor.indexOf(menor);
                int fin = menor.length();
                while (ini >= 0) {
                    mayor = this.agregarparentesis(mayor, ini, fin);
                    ini = mayor.indexOf(menor, ini + 2);
                    fin = menor.length();
                }
                mayor = mayor.replace(menor, der.get(k));
                this.der.set(i, mayor);
            }
            
        }
    }

    private static Integer obtenerprecedencia(Character c) {
        Integer valor = precedenciaOper.get(c);
        return valor == null ? 6 : valor;
    }

    private String agregarparentesis(String cadena, int pos, int fin) {
        String resultado="";
        for (int i = 0; i < cadena.length();i++) {
            if (i == pos) {
                resultado += "(";
            }
            resultado += cadena.charAt(i);
            if(i == (pos+fin-1)){
                resultado += ")";
            }
        }
        return resultado;
    }

    //gbrolo
    public static String aposfijo(String expresion) {
        String postfix = new String();
        Stack<Character> pila = new Stack<Character>();
        for (Character c : expresion.toCharArray()) {
            switch (c) {
                case '(':
                    pila.push(c);
                    break;
                case ')':
                    while (!pila.peek().equals('(')) {
                        postfix += pila.pop();
                    }
                    pila.pop();
                    break;
                default:
                    while (pila.size() > 0) {
                        Character caracter = pila.peek();
                        Integer precedenciacarac = obtenerprecedencia(caracter);
                        Integer actualpreccaract = obtenerprecedencia(c);
                        if (precedenciacarac >= actualpreccaract) {
                            postfix += pila.pop();
                        } else {
                            break;
                        }
                    }
                    pila.push(c);
                    break;
            }
        }
        while (pila.size() > 0)
            postfix += pila.pop();

        return postfix;
    }

    public void thompson() {
        for (int i = 0; i < derposfijo.size(); i++) {
            String expres = derposfijo.get(i);
            for (int j = 0; j < expres.length(); j++) {
                if (alfabeto.contains(expres.charAt(j)) || Character.toString(expres.charAt(j)).compareTo("$")==0) {
                    Estado inicial1 = new Estado(NFA.nroestados);
                    Estado final1 = new Estado(NFA.nroestados);
                    transicionesEstados.get(inicial1.getid()).addEstado(Character.toString(expres.charAt(j)), final1.getid());
                    pilainiciales.push(inicial1);
                    pilafinales.push(final1);
                } else if (obtenerprecedencia(expres.charAt(j)) == 2) { //unión
                    Estado inicial1 = pilainiciales.pop();
                    Estado inicial2 = pilainiciales.pop();
                    Estado final1 = pilafinales.pop();
                    Estado final2 = pilafinales.pop();
                    union(inicial1, inicial2, final1, final2);
                } else if (obtenerprecedencia(expres.charAt(j)) == 3) { //concatenación
                    auxfinal = pilafinales.pop();
                    Estado inicial1 = pilainiciales.pop();
                    Estado final1 = pilafinales.pop();
                    concatenacion(final1, inicial1);
                } else if (obtenerprecedencia(expres.charAt(j)) == 4) { //kleene
                    Estado inicial1 = pilainiciales.pop();
                    Estado final1 = pilafinales.pop();
                    kleene(inicial1, final1);
                }
                if (j == (expres.length() - 1)) {
                    Estado finalexp = pilafinales.peek();
                    this.estadoInicialAFN = pilainiciales.peek();
                    finalexp.definirFinal(true);
                    finalexp.definirToken(this.izq.get(i));
                    this.estadoFinalAFN = finalexp;
                    this.estadosfinales.add(finalexp);
                }
            }
            if (pilafinales.size()==2) {
                Estado inicial1 = pilainiciales.pop();
                Estado inicial2 = pilainiciales.pop();
                Estado final1 = pilafinales.pop();
                Estado final2 = pilafinales.pop();
                union(inicial1, inicial2, final1, final2);
                this.estadoInicialAFN = pilainiciales.peek();
                this.estadoFinalAFN = pilafinales.peek();
            }
        }
    }
    
    private void union(Estado inicial1, Estado inicial2, Estado final1, Estado final2) {
        Estado unioninicial = new Estado(NFA.nroestados);
        Estado unionfinal = new Estado(NFA.nroestados);
        transicionesEstados.get(unioninicial.getid()).addEstado("$", inicial1.getid());
        transicionesEstados.get(unioninicial.getid()).addEstado("$", inicial2.getid());
        transicionesEstados.get(final1.getid()).addEstado("$", unionfinal.getid());
        transicionesEstados.get(final2.getid()).addEstado("$", unionfinal.getid());
        pilainiciales.push(unioninicial);
        pilafinales.push(unionfinal);
    }

    private void concatenacion(Estado inicial1, Estado final1) {
        transicionesEstados.get(inicial1.getid()).addEstado("$", final1.getid());
        pilafinales.push(auxfinal);
        auxfinal = null;
    }

    private void kleene(Estado inicial, Estado final1) {
        Estado iniciokleene = new Estado(NFA.nroestados);
        Estado finkleene = new Estado(NFA.nroestados);
        transicionesEstados.get(final1.getid()).addEstado("$", inicial.getid());
        transicionesEstados.get(iniciokleene.getid()).addEstado("$", finkleene.getid());
        transicionesEstados.get(iniciokleene.getid()).addEstado("$", inicial.getid());
        transicionesEstados.get(final1.getid()).addEstado("$", finkleene.getid());
        pilainiciales.push(iniciokleene);
        pilafinales.push(finkleene);
    }

    public void imprimir() {
        try {
            PrintWriter writer = new PrintWriter("AFN.txt", "UTF-8");
            String s;
            writer.println("DEFINICIÓN REGULAR: ");
            imprimirDefinicion(this.der, writer);
            writer.println("---------------------------------------------");
            writer.println("");
            writer.println("DEFINICIONES REGULARES EN POSFIJO: ");
            imprimirDefinicion(this.derposfijo, writer);
            writer.println("---------------------------------------------");
            writer.println("");
            writer.println("----------AFN Generado con Thompson----------");
            String r;
            String t;
            s = "   |     |";
            for (Character c : alfabeto) {
                s = s + "     " + c + "     |";
            }
            r = "   =";
            t = "   -";
            for (int i = 0; i < s.length() - 4; i++) {
                r = r + "=";
                t = t + "-";
            }
            writer.println(r);
            writer.println(s);
            writer.println(r);
            for (int i = 0; i < NFA.transicionesEstados.size(); i++) {
                if (i < 10) {
                    s = "   |  " + i + "  |";
                } else {
                    s = "   |  " + i + " |";
                }
                Tabla aux = transicionesEstados.get(i);
                for (Character c : alfabeto) {
                    if (aux.getEstadoSet(Character.toString(c)).isEmpty()) {
                        s = s + "           |";
                    } else {
                        Set<Integer> setaux = aux.getEstadoSet(Character.toString(c));
                        s = s + "  " + setaux + " ";

                        for (Integer est : setaux) {
                            if (est < 10) {
                                s = s + " ";
                            }
                        }
                        if (setaux.size() == 1) {

                            s = s + "    |";
                        } else
                            s = s + "|";
                    }
                }
                writer.println(s);
                writer.println(t);
            }
            writer.println("ESTADO INICIAL: " + this.estadoInicialAFN);
            writer.println("ESTADO FINAL: " + this.estadoFinalAFN);
            writer.println("ESTADOS FINALES DE TOKENS");
            for (Estado e : this.estadosfinales) {
                writer.println("["+e.getid()+"] = "+e.getToken());
            }
            writer.println("----------------AFN-----------------");
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<String> getTokens() {
        return this.izq;
    }

    public List<Estado> getEstadosFinales() {
        return this.estadosfinales;
    }
    public void imprimirDefinicion(List<String> expresionesList, PrintWriter w) {
        String s = "Token->ExpresionRegular";
        w.println(s);
        for (int i = 0; i < this.getTokens().size(); i++) {
            s = this.getTokens().get(i) + "->" + expresionesList.get(i);
            w.println(s);
        }
    }
/* 	private void novisitados(Estado inicio) {
        for (Estado e : NFA.estados) {
            e.definirVisitado(false);
        }
	} */
}
