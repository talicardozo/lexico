package nfa;

/**
 * Estado genérico
 */
public class Estado {
    private int id;
    private boolean esInicial;
    private boolean esFinal;
    private String token;
    private boolean visitado;
    private int grupo;

    public Estado(int id) {
        this.id = id;
        this.esFinal = false;
        this.esInicial = false;
        this.visitado = false;
        Tabla tabla = new Tabla(NFA.alfabeto);
        NFA.transicionesEstados.put(id, tabla);
        NFA.estados.add(this);
        NFA.nroestados++;
    }
    
    public Estado(int id, boolean dfa) {
        this.id = id;
        this.esFinal = false;
        this.esInicial = false;
        this.visitado = false;
        this.token = null;
        this.grupo = -1;
    }

    public String toString() {
        return String.valueOf(this.id);
    }

    public int getid() {
        return this.id;
    }

    public void setid(int id) {
        this.id = id;
    }

    public int getgrupo() {
        return this.grupo;
    }

    public void setgrupo(int grupo) {
        this.grupo = grupo;
    }

    public void definirInicial(boolean esInicial) {
        this.esInicial = esInicial;
    }

    public boolean getInicial() { 
        return this.esInicial; 
    }

    public void definirToken(String t) {
        this.token = t;
    }

    public String getToken() {
        return this.token;
    }

    public void definirFinal(boolean esFinal) {
        this.esFinal = esFinal;
    }

    public boolean getFinal() {
        return this.esFinal;
    }

    public void definirVisitado(boolean esVisitado) {
        this.visitado = esVisitado;
    }

    public boolean getVisitado() {
        return this.visitado;
    }
}