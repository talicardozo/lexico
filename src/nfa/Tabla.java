package nfa;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Tabla {
    private Map<String, Set<Integer>> tabla;

    public Tabla(List<Character> alfabeto) {
        this.tabla = new HashMap<String, Set<Integer>>();
        for (Character c : alfabeto) {
            Set<Integer> estados = new HashSet<Integer>();
            this.tabla.put(Character.toString(c), estados);
        }
    }

    public void addEstado(String a, Integer e) {
        //Character a = s.charAt(0);
        this.tabla.get(a).add(e);
    }

    public void getEstados(String a, Integer e) {
        //Character a = s.charAt(0);
        this.tabla.get(a).add(e);
    }
    
    public Integer getLongitud() {
        return this.tabla.size();
    }

    public Set<Integer> getEstadoSet(String c) {
        return this.tabla.get(c);
    }
    public Map<String, Set<Integer>> getTabla() { return this.tabla; }
}